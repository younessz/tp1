package fr.zorgan.tp1.data;

public class Country {

    private String name;
    private String capital;
    private String imgFile;
    private String language;
    private String currency;
    private int population;
    private int area;

    public Country(String name, String capital, String imgFile, String language, String currency, int population, int area) {
        this.name = name;
        this.capital = capital;
        this.imgFile = imgFile;
        this.language = language;
        this.currency = currency;
        this.population = population;
        this.area = area;
    }

    public String getName() {
        return name;
    }

    public void setName(String capital) {
        this.name = name;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public String getImgUri() {
        return "@drawable/"+imgFile;
    }

    public void setImgFile(String imgFile) {
        this.imgFile = imgFile;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    public int getArea() {
        return area;
    }

    public void setArea(int area) {
        this.area = area;
    }


    public static Country[] countries = {
            new Country("France", "Paris", "france_drapeau", "français", "euro", 67348000, 640679),
            new Country("Allemagne", "Berlin", "allemagne_drapeau", "allemand", "euro", 82887000, 357340),
            new Country("Espagne", "Madrid", "espagne_drapeau", "espagnol", "euro", 48958159, 505911),
            new Country("Afrique du Sud", "Pretoria", "afrique_drapeau", "anglais, afrikaans et 9 langues bantoues", "rand", 55653654, 1219912),
            new Country("États-Unis", "Washington", "etats_drapeau", "aucune", "dollar américain", 327167434, 9833517),
            new Country("Japon", "Tokyo", "japan_drapeau", "japonais", "yen", 126168156, 377915)};
}
